{{--
  Template Name: Home
--}}

@extends('layouts.app')  

@section('content')

  {{-- MOBILE MENU --}}
  <div class="menu-mobile-box">
    <nav class="menu-nav">
      <ul class="menu menu__list" role="menu" aria-labelledby="mobile-menu">
        <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>
        <li role="menuitem" class="menu__item menu__item-sobre"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
        <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
        <li role="menuitem" class="menu__item menu__item-inicio"><span class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></span></li>
        <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
        <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
        <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
      </ul>
    </nav>        
  </div>

  {{--INICIO --}}
  <section id="inicio" class="section section-inicio text--white text--shadow"> 
    <div class="container">
      <div class="row">
        {{-- mobile btn--}}
        <button class="d-xl-none hamburger hamburger--squeeze menu-mobile__btn" type="button" aria-controls="mobile-nav" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>  
        {{-- desktop menu--}}
        <div class="col-12 col-md-10 menu-box wow fadeIn">
          <nav class="menu-nav">
            <ul class="menu menu__list" role="menubar">
              <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>
              <li role="menuitem" class="menu__item menu__item-sobre"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
              <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
              <li role="menuitem" class="menu__item menu__item-inicio"><h1 class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></h1></li>
              <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
              <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
              <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
            </ul>
          </nav>        
        </div>
      </div>
      <div class="home-box">
        <h3 class="wow zoomIn fadeIn global__subtitle home__subtitle">O local perfeito para seu</h3>
        <h2 class="wow zoomIn fadeIn global__title home__title">SONHO</h2>
        <h3 class="wow zoomIn fadeIn global__subtitle home__subtitle">Se tornar realidade!</h3>  
      </div>
      <div class="mouse-scroll-box">
        <div class="mouse-scroll"></div>
      </div>
    </div>
  </section>
  
  {{-- SOBRE --}}
  <section id="sobre" class="section section-sobre text--black"> 
    <div class="container">
      <div class="row">
        {{-- mobile btn--}}
        <button class="d-xl-none hamburger hamburger--squeeze menu-mobile__btn" type="button" aria-controls="mobile-nav" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>  
        {{-- desktop menu--}}
        <div class="col-12 col-md-10 menu-box wow fadeIn">
          <nav class="menu-nav">
            <ul class="menu menu__list" role="menubar">
              <li role="menuitem" class="menu__item menu__item-inicio"><span class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></span></li>
              <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>
              <li role="menuitem" class="menu__item menu__item-sobre"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
              <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
              <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
              <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
              <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
            </ul>
          </nav>        
        </div>
      </div>
      
      <div class="row justify-content-center about-box">
        <div class="col-12 col-md-10 col-xl-8 about__description-box">
          <h3 class="wow fadeInUp global__subtitle about__subtitle">Saiba mais sobre o</h3>
          <h2 class="wow fadeInUp global__title about__title">Le Partie</h2>
          <div class="wow fadeInUp about__description">
            <p>{{get_post_meta(get_the_ID(), '_lepartie_about_description', true )}}</p>
          </div>
          <figure class="wow fadeIn img__box img__box--wide about__featured-image-box">
            @php 
              $img_id = get_post_meta(get_the_ID(), '_lepartie_about_featured-image_id', true);
              $img_src = wp_get_attachment_image_url($img_id, 'lepartie-large');
              $img_srcset = wp_get_attachment_image_srcset( $img_id, 'lepartie-large');
              $img_sizes = wp_get_attachment_image_sizes( $img_id, 'lepartie-large');
              $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
              if(empty($img_alt)) {
                $image_alt = 'Le Partie Comemorações - Pathernon';
              }
            @endphp
            <img data-srcset="{{$img_srcset}}" data-sizes="{{$img_sizes}}" data-src="{{$img_src}}" alt="{{$img_alt}}" class="lozad img-fluid about__featured-image">
          </figure>
        </div>
        @if (get_post_meta(get_the_ID(), '_lepartie_about_testimonials_show', 1))
        @php $testimonials = get_post_meta(get_the_ID(), '_lepartie_about_testimonials', true); @endphp
        <div class="col-12 col-md-10 col-xl-8 about__testimonials-box">
          <h3 class="global__subtitle wow fadeInUp about__testimonials__subtitle">O que as pessoas tem falado</h3>
          <h2 class="global__title wow fadeInUp about__testimonials__title">Sobre Nós</h2>
          <div class="wow fadeIn about__testimonials-slider">
            <ul class="slides about__testimonials-list">
              @foreach ((array) $testimonials as $key => $testimonial)
                @php 
                  $img_id = $profile = $description = ''; 
                      
                  if(isset($testimonial['profile'])) {
                    $profile = $testimonial['profile'];
                  }
                  
                  if(isset($testimonial['profile-picture_id'])) {
                    $img_id = $testimonial['profile-picture_id'];
                    $img_src = wp_get_attachment_image_url($img_id, 'lepartie-profile');
                    $img_srcset = wp_get_attachment_image_srcset( $img_id, 'lepartie-profile');
                    $img_sizes = wp_get_attachment_image_sizes( $img_id, 'lepartie-profile');
                    $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                    if(empty($img_alt)) {
                      $image_alt = $profile;
                    }
                  }
    
                  if(isset($testimonial['description'])) {
                    $description = $testimonial['description'];
                  }
                @endphp

                <li class="about__testimonial-item">
                  <figure class="img__box about__testimonial__profile-picture-box">
                    <img data-src="{{$img_src}}" alt="{{$img_alt}}" class="lozad img-center about__testimonial__profile-picture">
                  </figure>
                  <div class="about__testimonial__profile-content">
                    <p class="about__testimonial__description">{{$description}}</p>
                    <p class="about__testimonial__profile">{{$profile}}</p>
                  </div>
                </li>
              @endforeach
            </ul>
          </div>
          <div class="about__testimonials-slider__custom-navigation">
            <a href="javascript:;" class="flex-prev"><</a>
            <div class="custom-controls-container"></div>
            <a href="javascript:;" class="flex-next">></a>
          </div>
        </div>
        @endif
      </div>
    </div>
  </section>

  {{-- ESTRUTURA --}}
  <section id="estrutura" class="section section-estrutura text--white">
    <div class="container">
      <div class="row">
        {{-- mobile btn--}}
        <button id="mobile-menu" class="d-xl-none hamburger hamburger--squeeze menu-mobile__btn" type="button"  aria-controls="mobile-nav" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>  
        {{-- desktop menu--}}
        <div class="col-12 col-md-10 menu-box wow fadeIn text--shadow">
          <nav class="menu-nav">
            <ul class="menu menu__list" role="menubar">
              <li role="menuitem" class="menu__item menu__item-inicio"><span class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></span></li>
              <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>  
              <li role="menuitem" class="menu__item menu__item-sobre active"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
              <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
              <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
              <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
              <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
            </ul>
          </nav>        
        </div>
      </div>
      <div class="row justify-content-center align-items-center structure-box">
        <div class="col-12 col-md-10 col-xl-5 d-flex flex-wrap justify-content-center structure__description-box ">
          <h3 class="wow fadeInUp global__subtitle structure__subtitle">Conheça nossa estrutura</h3>
          <h2 class="wow fadeInUp global__title structure__title">Moderna &amp; </br>Elegante</h2>
          <div class="wow fadeInUp structure__description">
            {{get_post_meta( get_the_ID(), '_lepartie_structure_description', true )}}
          </div>
          <a data-fancybox="structure" data-src="#structure__modal" href="javascript:;" class="wow fadeIn global__btn global__btn--white-black structure__btn-more" role="button">Mais</a>
        </div>
        <div class="col-12 col-md-10 col-xl-5 justify-content-center structure__gallery-box">
          @php 
            $files = get_post_meta(get_the_ID(), '_lepartie_structure_gallery', true); 
            $gallery_img = array();

            if (isset($files)) {
              foreach ((array)$files as $img_id => $attachment_url) {
                if(isset($img_id)) {
                  $img_src = wp_get_attachment_image_url($img_id, 'lepartie-medium-square');
                  $img_srcset = wp_get_attachment_image_srcset( $img_id, 'lepartie-extra-large');
                  $img_sizes = wp_get_attachment_image_sizes( $img_id, 'lepartie-extra-large');
                  $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);

                  $gallery_img[] = array('src' => $img_src, 'srcset' => $img_srcset, 'sizes' => $img_sizes, 'alt' => $img_alt);
                }
              }
            }
          @endphp
          <div class="container-grid justify-content-center justify-content-xl-end">
            @foreach($gallery_img as $img)
              {{--<a data-fancybox="structure" 
                data-srcset="{{$img['srcset']}}" 
                href="{{$img['src']}}" class="img__box__link">
                <div class="img__box img__box--square structure__gallery__img-box">
                  <img src="{{$img['src']}}" alt="{{$img['alt']}}" class="img-center--zoom structure__gallery__img"> 
                </div>  
              </a>--}}
              <figure class="wow fadeIn img__box img__box--square structure__gallery__img-box">
                <img data-src="{{$img['src']}}" {{--data-srcset="{{$img['srcset']}}" data-sizes="{{$img['sizes']}}" --}} alt="{{$img['alt']}}" class="lozad img-center structure__gallery__img"> 
              </figure>
            @endforeach  
          </div>
        </div>
      </div>
    </div>
  </section>

  {{-- FOTOS --}}
  <section id="fotos" class="section section-fotos text--black">
    <div class="container">
      <div class="row">
        {{-- mobile btn--}}
        <button class="d-xl-none hamburger hamburger--squeeze menu-mobile__btn" type="button" aria-controls="mobile-nav" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>  
        {{-- desktop menu--}}
        <div class="col-12 col-md-10 menu-box wow fadeIn">
          <nav class="menu-nav">
            <ul class="menu menu__list" role="menubar">
              <li role="menuitem" class="menu__item menu__item-inicio"><span class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></span></li>
              <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>
              <li role="menuitem" class="menu__item menu__item-sobre active"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
              <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
              <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
              <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
              <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
            </ul>
          </nav>        
        </div>  
      </div>
      <div class="row justify-content-center align-items-center photos-box">
        <div class="col-12 col-md-10">
          <h3 class="wow fadeInUp global__subtitle photos__subtitle">Veja mais detalhes do Le Partie</h3>
          <h2 class="wow fadeInUp global__title photos__title">Fotos</h2>
          <div class="photos__gallery-box">
            @php 
            $files = get_post_meta(get_the_ID(), '_lepartie_photos_gallery', true); 
            $gallery_img = array();

            if (isset($files)) {
              foreach ((array)$files as $img_id => $attachment_url) {
                if(isset($img_id)) {
                  $img_src = wp_get_attachment_image_url($img_id, 'lepartie-medium');
                  $img_srcset = wp_get_attachment_image_srcset( $img_id, 'lepartie-extra-large');
                  $img_sizes = wp_get_attachment_image_sizes( $img_id, 'lepartie-extra-large');
                  $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                  $gallery_img[] = array('src' => $img_src, 'srcset' => $img_srcset, 'sizes' => $img_sizes, 'alt' => $img_alt);
                }
              }
            }
            @endphp
            <div class="container-grid justify-content-center">
              @php $count_item = 1 @endphp
              @foreach($gallery_img as $img)
                @php 
                  $class_item = ''; 
                  $class_box = 'img__box--square';
                @endphp
                @switch($count_item)
                  @case(2)
                    @php 
                      $class_item .= 'grid-item--height2'; 
                      $class_box = 'img__box--h2';
                    @endphp
                    @break
                  @case(3)
                    @php 
                      $class_item .= 'grid-item--width2'; 
                      $class_box = 'img__box--w2';
                    @endphp
                    @break
                  @case(9)
                    @php 
                      $class_item .= 'grid-item--width2'; 
                      $class_box = 'img__box--w2';
                    @endphp
                    @break
                @endswitch
                <a data-fancybox="photos" 
                    data-srcset="{{$img['srcset']}}" 
                    href="{{$img['src']}}" class="wow fadeIn {{$class_item}} img__box__link">
                  <div class="img__box img__box--square {{$class_box}} photos__gallery__img-box">
                    <img data-src="{{$img['src']}}" data-srcset="{{$img['srcset']}}" data-sizes=""="{{$img['sizes']}}" alt="{{$img['alt']}}" class="lozad img-center--zoom structure__gallery__img"> 
                  </div>  
                </a>
                  @php $count_item++ @endphp
                  @if ($count_item > 9)
                    @php $count_item = 1 @endphp
                  @endif
                @endforeach  
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {{-- CONTATO --}}
  <section id="contato" class="section section-contato text--black">
    <div class="container">
      <div class="row">
        {{-- mobile btn--}}
        <button class="d-xl-none hamburger hamburger--squeeze menu-mobile__btn" type="button" aria-controls="mobile-nav" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>  
        {{-- desktop menu--}}
        <div class="col-12 col-md-10 menu-box wow fadeIn">
          <nav class="menu-nav">
            <ul class="menu menu__list" role="menubar">
              <li role="menuitem" class="menu__item menu__item-inicio"><span class="site-name"><a href="#inicio" class="menu__link menu__link-inicio"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></a></span></li>
              <li role="menuitem" class="menu__item menu__item-home"><a href="#inicio" class="menu__link menu__link-home">Home</a></li>
              <li role="menuitem" class="menu__item menu__item-sobre active"><a href="#sobre" class="menu__link menu__link-sobre">Sobre</a></li>
              <li role="menuitem" class="menu__item menu__item-estrutura"><a href="#estrutura" class="menu__link menu__link-estrutura">Estrutura</a></li>
              <li role="menuitem" class="menu__item menu__item-fotos"><a href="#fotos" class="menu__link menu__link-fotos">Fotos</a></li>
              <li role="menuitem" class="menu__item menu__item-tour"><span class="menu__link menu__link-tour">Tour Virtual</span></li>
              <li role="menuitem" class="menu__item menu__item-contato"><a href="#contato" class="menu__link menu__link-contato">Contato</a></li>
            </ul>
          </nav>        
        </div>
      </div>
      <div class="row justify-content-center align-items-end contact-box">
        <div class="wow fadeInRight col-12 col-md-10 order-xl-1 col-xl-5 contact__form-box form">
          <h2 class="global__title contact__title">Contato</h2>
          <h3 class="global__subtitle contact__subtitle">Para dúvidas ou maiores informações, envie-nos uma mensagem, que em breve entraremos em contato</h3>
          @php dynamic_sidebar('sidebar-contact-form') @endphp
        </div>
        <div class="wow fadeInLeft col-12 col-md-10 order-xl-0 col-xl-5 contact__address-box">
          <div class="contact__address__wrap">
            <div class="img__box img__box--square contact__map">
              <iframe class="lozad" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1839.4322130620544!2d-47.311630543016385!3d-22.77041205847597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c899c78aeee3bf%3A0xfd992d01c63b3db2!2sLe+Partie+comemora%C3%A7%C3%B5es!5e0!3m2!1spt-BR!2sbr!4v1525941882438" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="contact__address__content">
              <div class="contact__address__info">
                @php
                  $contact_address = '<span>'.get_post_meta(get_the_ID(), '_lepartie_contact_address', true).'</span>';
                  $contact_phones = '<span>'.get_post_meta(get_the_ID(), '_lepartie_contact_phone', true) .'</span><span>/ '. get_post_meta(get_the_ID(), '_lepartie_contact_cell-phone', true).'</span>';
                @endphp
                <p class="contact__address-text">{!!$contact_address!!}</p>
                <p class="contact__phones-text">Fones: {!!$contact_phones!!}</p>
              </div>
              <div class="contact__address__btn-directions-box">
                <a class="global__btn global__btn--black-white contact__btn-directions" role="button" href="https://www.google.com/maps/dir//Le+Partie+comemora%C3%A7%C3%B5es,+R.+Darcy+Carrion,+Parque+Industrial+Harmonia,+121+-+Parque+Industrial+Harmonia,+Nova+Odessa+-+SP,+13460-000/@-22.770428,-47.3455556,13z/?hl=pt-BR">Como chegar</a>
              </div>
            </div>
          </div>  
        </div>     
      </div>
    </div>
    <div class="container-fluid footer-box">
      <div class="row justify-content-center">
        @include('partials.footer')
      </div>
    </div>
  </section>

  {{-- ESTRUTURA MODAL --}}
  <div class="modal__content structure__modal__content" id="structure__modal">
    <button data-fancybox-close="" class="btn hamburger hamburger--squeeze fancybox__close__btn is-active">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>
    <div class="container">
        <div class="row">
          <div class="col-12 col-md-10 modal__logo-box">
            <nav class="modal__logo-nav">
              <ul class="modal__logo__list" >
                <li class="modal__logo__item modal__logo__item-inicio"><span class="site-name"><span class="modal__link"><img src="@asset('images/lepartie-logo.svg')" alt="Le Partie Comemorações" class="header__logo"></span></span></li>              </ul>
            </nav>        
          </div>
        </div>
      <div class="row justify-content-center structure__modal__gallery-box">
        <div class="col-12 col-md-10 justify-content-center">
          <h3 class="wow fadeInUp text-center global__subtitle structure__gallery__subtitle">@php echo get_post_meta( get_the_ID(), '_lepartie_structure_gallery_subtitle', true) @endphp</h3>
          <h2 class="wow fadeInUp text-center global__title structure__gallery__title">@php echo get_post_meta( get_the_ID(), '_lepartie_structure_gallery_title', true) @endphp</h2>
          @php
            $categories = get_post_meta( $post->ID, '_lepartie_structure_category', true)
          @endphp
          @if ($categories)
            <ul class="justify-content-center structure__gallery__categories-list">
              @foreach ((array) $categories as $key => $category) 
                @if ($loop->first)
                  <li class="wow fadeIn structure__gallery__categories-item current" data-category="0">
                    <span class="structure__gallery__category__link category__link-todas">Todas</span>
                  </li>
                @endif
                @if (isset($category['title']))  
                  @php $title = esc_html($category['title']) @endphp
                  <li class="wow fadeIn structure__gallery__categories-item" data-category="{{$loop->iteration}}">
                    <span class="structure__gallery__category__link category__link-{{preg_replace('/\s+/', '_', strtolower(trim($title)))}}">@php echo $title @endphp</span></li>
                  </li>
                @endif
              @endforeach
            </ul>

            <div class="container-grid justify-content-center">
              @foreach ((array) $categories as $key => $category) 
                @php
                  $images = $category['images'];
                  $category_id = $loop->iteration;
                @endphp  

                @if (isset($images)) 
                  @foreach ((array)$images as $img_id => $attachment_url) 
                    @if(isset($img_id)) 
                      @php
                        $img_src = wp_get_attachment_image_url($img_id, 'lepartie-medium-square');
                        $img_srcset = wp_get_attachment_image_srcset( $img_id, 'lepartie-extra-large');
                        $img_sizes = wp_get_attachment_image_sizes( $img_id, 'lepartie-extra-large');
                        $img_alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
                        $img_desc = get_post_field( 'post_content', $img_id);
                      @endphp
                      <a data-fancybox="structure-gallery" data-srcset="{{$img_srcset}}" data-caption="{{$img_desc}}" href="{{$img_src}}" class="img__box__link structure__gallery__category structure__gallery__category--{{$category_id}}">
                        <div class="img__box img__box--square structure__modal__gallery__img-box">
                          <img data-src="{{$img_src}}" {{-- data-srcset="{{$img_srcset}}" data-sizes=""="{{$img_sizes}}" --}} alt="{{$img_alt}}" class="lozad img-center structure__modal__gallery__img-box"> 
                          @if(!empty($img_desc))
                            <div class="structure__img__desc-box">
                              <span class="structure__img__desc">@php echo $img_desc @endphp</span>
                            </div>
                          @endif
                        </div>  
                      </a>
                    @endif
                  @endforeach
                @endif
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>

  {{-- TOUR MODAL--}}
  <div class="modal__content tour__modal__content" id="tour__modal">
    <button data-fancybox-close="" class="btn hamburger hamburger--squeeze fancybox__close__btn is-active">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>  
  <iframe src="" frameborder="0" class="tour__iframe"></iframe>
  </div>
@endsection
