<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="profile" href="http://gmpg.org/xfn/11"/>

  {{-- preconnects --}}
  <link rel="preconnect" href="https://code.jquery.com/" crossorigin>
  <link rel="preconnect" href="https://www.google.com/" crossorigin>
  <link rel="preconnect" href="https://khms1.googleapis.com/" crossorigin>
  <link rel="preconnect" href="https://maps.googleapis.com/" crossorigin>
  <link rel="preconnect" href="https://maps.gstatic.com/" crossorigin>
  
  {{-- Sections backgrounds --}}
  @php
    $post_ID = get_queried_object_id();
    $home_bg_id = get_post_meta($post_ID, '_lepartie_home_bg-image_id', true);
    $about_bg_id = get_post_meta($post_ID, '_lepartie_about_bg-image_id', true);
    $structure_bg_id = get_post_meta($post_ID, '_lepartie_structure_bg-image_id', true);
    $structure_modal_bg_id = get_post_meta($post_ID, '_lepartie_structure_gallery-bg-image_id', true);
    $photos_bg_id = get_post_meta($post_ID, '_lepartie_photos_bg-image_id', true);
    $contact_bg_id = get_post_meta($post_ID, '_lepartie_contact_bg-image_id', true);
    $preload_bg = '';
    $style_bg = '<style type="text/css">';
  @endphp
  
  {{-- DEFAULT--}}
  
  {{-- home --}} 
  @php $home_bg_url = wp_get_attachment_image_url($home_bg_id, 'lepartie-medium-square') @endphp
  @if ($home_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$home_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#inicio {background-image: url('.$home_bg_url.');}';
    @endphp
  @endif

  {{-- about --}} 
  @php $about_bg_url = wp_get_attachment_image_url($about_bg_id, 'lepartie-medium-square') @endphp
  @if ($about_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$about_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#sobre {background-image: url('.$about_bg_url.');}';
    @endphp
  @endif

  {{-- structure --}} 
  @php $structure_bg_url = wp_get_attachment_image_url($structure_bg_id, 'lepartie-medium-square') @endphp
  @if ($structure_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#estrutura {background-image: url('.$structure_bg_url.');}';
    @endphp
  @endif

  {{-- structure_modal --}} 
  @php $structure_modal_bg_url = wp_get_attachment_image_url($structure_modal_bg_id, 'lepartie-medium-square') @endphp
  @if ($structure_modal_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_modal_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#structure__modal {background-image: url('.$structure_modal_bg_url.');}';
    @endphp
  @endif

  {{-- photos --}} 
  @php $photos_bg_url = wp_get_attachment_image_url($photos_bg_id, 'lepartie-medium-square') @endphp
  @if ($photos_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$photos_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#fotos {background-image: url('.$photos_bg_url.');}';
    @endphp
  @endif

  {{-- contact --}} 
  @php $contact_bg_url = wp_get_attachment_image_url($contact_bg_id, 'lepartie-medium-square') @endphp
  @if ($contact_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$contact_bg_url.'" as="image" media="(max-width: 960px)">';
      $style_bg .=  '#contato {background-image: url('.$contact_bg_url.');}';
    @endphp
  @endif

  {{-- MEDIUM--}}
  
  {{-- home --}} 
  @php 
    $style_bg .= '@media (min-width: 961px) {';
    $home_bg_url = wp_get_attachment_image_url($home_bg_id, 'lepartie-medium');
  @endphp
  @if ($home_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$home_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#inicio {background-image: url('.$home_bg_url.');}';
    @endphp
  @endif

  {{-- about --}} 
  @php $about_bg_url = wp_get_attachment_image_url($about_bg_id, 'lepartie-medium') @endphp
  @if ($about_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$about_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#sobre {background-image: url('.$about_bg_url.');}';
    @endphp
  @endif

  {{-- structure --}} 
  @php $structure_bg_url = wp_get_attachment_image_url($structure_bg_id, 'lepartie-medium') @endphp
  @if ($structure_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#estrutura {background-image: url('.$structure_bg_url.');}';
    @endphp
  @endif

  {{-- structure_modal --}} 
  @php $structure_modal_bg_url = wp_get_attachment_image_url($structure_modal_bg_id, 'lepartie-medium') @endphp
  @if ($structure_modal_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_modal_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#structure__modal {background-image: url('.$structure_modal_bg_url.');}';
    @endphp
  @endif

  {{-- photos --}} 
  @php $photos_bg_url = wp_get_attachment_image_url($photos_bg_id, 'lepartie-medium') @endphp
  @if ($photos_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$photos_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#fotos {background-image: url('.$photos_bg_url.');}';
    @endphp
  @endif

  {{-- contact --}} 
  @php $contact_bg_url = wp_get_attachment_image_url($contact_bg_id, 'lepartie-medium') @endphp
  @if ($contact_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$contact_bg_url.'" as="image" media="(min-width: 961px) and (max-width: 1366px)">';
      $style_bg .=  '#contato {background-image: url('.$contact_bg_url.');}';
    @endphp
  @endif
  @php $style_bg .= '}' @endphp 
  
  {{-- LARGE--}}
  
  {{-- home --}} 
  @php 
    $style_bg .= '@media (min-width: 1367px) {';
    $home_bg_url = wp_get_attachment_image_url($home_bg_id, 'lepartie-large');
  @endphp
  @if ($home_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$home_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#inicio {background-image: url('.$home_bg_url.');}';
    @endphp
  @endif

  {{-- about --}} 
  @php $about_bg_url = wp_get_attachment_image_url($about_bg_id, 'lepartie-large') @endphp
  @if ($about_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$about_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#sobre {background-image: url('.$about_bg_url.');}';
    @endphp
  @endif

  {{-- structure --}} 
  @php $structure_bg_url = wp_get_attachment_image_url($structure_bg_id, 'lepartie-large') @endphp
  @if ($structure_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#estrutura {background-image: url('.$structure_bg_url.');}';
    @endphp
  @endif

  {{-- structure_modal --}} 
  @php $structure_modal_bg_url = wp_get_attachment_image_url($structure_modal_bg_id, 'lepartie-large') @endphp
  @if ($structure_modal_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_modal_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#structure__modal {background-image: url('.$structure_modal_bg_url.');}';
    @endphp
  @endif

  {{-- photos --}} 
  @php $photos_bg_url = wp_get_attachment_image_url($photos_bg_id, 'lepartie-large') @endphp
  @if ($photos_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$photos_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#fotos {background-image: url('.$photos_bg_url.');}';
    @endphp
  @endif

  {{-- contact --}} 
  @php $contact_bg_url = wp_get_attachment_image_url($contact_bg_id, 'lepartie-large') @endphp
  @if ($contact_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$contact_bg_url.'" as="image" media="(min-width: 1367px) and (max-width: 1920px)">';
      $style_bg .=  '#contato {background-image: url('.$contact_bg_url.');}';
    @endphp
  @endif
  @php $style_bg .= '}' @endphp

  {{-- EXTRA-LARGE--}}
  
  {{-- home --}} 
  @php 
    $style_bg .= '@media (min-width: 1921px) {';
    $home_bg_url = wp_get_attachment_image_url($home_bg_id, 'lepartie-extra-large');
  @endphp
  @if ($home_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$home_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#inicio {background-image: url('.$home_bg_url.');}';
    @endphp
  @endif

  {{-- about --}} 
  @php $about_bg_url = wp_get_attachment_image_url($about_bg_id, 'lepartie-extra-large') @endphp
  @if ($about_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$about_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#sobre {background-image: url('.$about_bg_url.');}';
    @endphp
  @endif

  {{-- structure --}} 
  @php $structure_bg_url = wp_get_attachment_image_url($structure_bg_id, 'lepartie-extra-large') @endphp
  @if ($structure_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#estrutura {background-image: url('.$structure_bg_url.');}';
    @endphp
  @endif

  {{-- structure_modal --}} 
  @php $structure_modal_bg_url = wp_get_attachment_image_url($structure_modal_bg_id, 'lepartie-extra-large') @endphp
  @if ($structure_modal_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_modal_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#structure__modal {background-image: url('.$structure_modal_bg_url.');}';
    @endphp
  @endif

  {{-- photos --}} 
  @php $photos_bg_url = wp_get_attachment_image_url($photos_bg_id, 'lepartie-extra-large') @endphp
  @if ($photos_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$photos_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#fotos {background-image: url('.$photos_bg_url.');}';
    @endphp
  @endif

  {{-- contact --}} 
  @php $contact_bg_url = wp_get_attachment_image_url($contact_bg_id, 'lepartie-extra-large') @endphp
  @if ($contact_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$contact_bg_url.'" as="image" media="(min-width: 1921px) and (max-width: 3840px)">';
      $style_bg .=  '#contato {background-image: url('.$contact_bg_url.');}';
    @endphp
  @endif
  @php $style_bg .= '}' @endphp

  {{-- FULL--}}
  
  {{-- home --}} 
  @php 
    $style_bg .= '@media (min-width: 3841px) {';
    $home_bg_url = wp_get_attachment_image_url($home_bg_id, 'full');
  @endphp
  @if ($home_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$home_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#inicio {background-image: url('.$home_bg_url.');}';
    @endphp
  @endif

  {{-- about --}} 
  @php $about_bg_url = wp_get_attachment_image_url($about_bg_id, 'full') @endphp
  @if ($about_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$about_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#sobre {background-image: url('.$about_bg_url.');}';
    @endphp
  @endif

  {{-- structure --}} 
  @php $structure_bg_url = wp_get_attachment_image_url($structure_bg_id, 'full') @endphp
  @if ($structure_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#estrutura {background-image: url('.$structure_bg_url.');}';
    @endphp
  @endif

  {{-- structure_modal --}} 
  @php $structure_modal_bg_url = wp_get_attachment_image_url($structure_modal_bg_id, 'full') @endphp
  @if ($structure_modal_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$structure_modal_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#structure__modal {background-image: url('.$structure_modal_bg_url.');}';
    @endphp
  @endif

  {{-- photos --}} 
  @php $photos_bg_url = wp_get_attachment_image_url($photos_bg_id, 'full') @endphp
  @if ($photos_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$photos_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#fotos {background-image: url('.$photos_bg_url.');}';
    @endphp
  @endif

  {{-- contact --}} 
  @php $contact_bg_url = wp_get_attachment_image_url($contact_bg_id, 'full') @endphp
  @if ($contact_bg_url) 
    @php
      $preload_bg .= '<link rel="preload" href="'.$contact_bg_url.'" as="image" media="(min-width: 3841px)">';
      $style_bg .=  '#contato {background-image: url('.$contact_bg_url.');}';
    @endphp
  @endif
  
  @php echo $style_bg.'}</style>' @endphp

  {{-- preloads--}}
  @php echo $preload_bg @endphp
  <link rel="preload" href="@asset('styles/main.css')" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="@asset('images/lepartie-logo.svg')" as="image" type="image/svg+xml">
  <link rel="preload" href="@asset('fonts/Isabel-Regular.woff2')" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="@asset('fonts/Isabel-Light.woff2')" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="@asset('fonts/GothamPro.woff2')" as="font" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="@asset('fonts/GothamPro-Light.woff2')" as="font" type="font/woff2" crossorigin="anonymous">

  {{-- Favicons --}}
  {{-- generics --}}
  <link rel="icon" href="@asset('images/favicon-ico-32.png')" sizes="32x32" type="image/png">
  <link rel="icon" href="@asset('images/favicon-ico-57.png')" sizes="57x57" type="image/png">
  <link rel="icon" href="@asset('images/favicon-76.png')" sizes="76x76" type="image/png">
  <link rel="icon" href="@asset('images/favicon-96.png')" sizes="96x96" type="image/png">
  <link rel="icon" href="@asset('images/favicon-128.png')" sizes="128x128" type="image/png">
  <link rel="icon" href="@asset('images/favicon-228.png')" sizes="228x228" type="image/png">
  {{-- Android --}}
  <link rel="shortcut icon" href="@asset('images/favicon-196.png')" sizes="196x196" type="image/png">
  {{-- iOS --}}
  <link rel="mask-icon" href="@asset('images/lepartie-logo.svg')" color="#000">
  <link rel="apple-touch-icon" href="@asset('images/favicon-120.png')" sizes="120x120" type="image/png"> 
  <link rel="apple-touch-icon"  href="@asset('images/favicon-152.png')" sizes="152x152" type="image/png">
  <link rel="apple-touch-icon" href="@asset('images/favicon-180.png')" sizes="180x180" type="image/png">

  @php wp_head() @endphp
</head>
