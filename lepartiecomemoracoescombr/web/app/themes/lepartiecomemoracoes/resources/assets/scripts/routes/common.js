import lozad from 'lozad';
import WOW from 'wow.js';

export default {
  init() {
    // JavaScript to be fired on all pages
    IntersectionObserver.prototype.POLL_INTERVAL = 100;

    //lazy load
    const observer = lozad('.lozad', {
      rootMargin: '1000px 1000px',
      threshold: 0.1,
    });
    observer.observe();


    $(window).load(function () {
      //Flexslider
      $('.about__testimonials-slider').flexslider({
        animation: "slide",
        animationSpeed: 500,
        slideshowSpeed: 15000,
        //controlNav: false,
        controlsContainer: $(".about__testimonials-slider__custom-navigation"),
        customDirectionNav: $(".about__testimonials-slider__custom-navigation a"),
      });
    });

    //General Animations
    let wow = new WOW({
      boxClass: 'wow', 
      animateClass: 'animated', 
      offset: 0, 
      mobile: true, 
      live: true,
    });
    wow.init();

  },
  finalize() {

    // JavaScript to be fired on all pages, after page specific JS is fired
    //add section classes
    var sections = [ "inicio", "sobre", "estrutura", "fotos", "contato" ];
    var bodyEl = $("body");
    var mainEl = document.getElementById("fullpage");
    $(window).on("scroll", function() {
        var scrollTop = $(this).scrollTop();
        $("section").each(function() {
            var el = $(this),
                className = el.attr("id");
            if (($.inArray(className, sections) > -1) && (scrollTop >= (el.offset().top -50))) {
                bodyEl.addClass(className);  
                mainEl.className = "main viewing-" + className;
            } else {
                bodyEl.removeClass(className);
            }
        });
    });

    //mobile-menu
    $(".menu-mobile__btn").click(function () {
      $(this).toggleClass("is-active");
      $('body').toggleClass("lock-scroll");
      $('.menu-mobile-box').toggleClass("is-active d-flex animated faster fadeIn");
      $('.menu__list').toggleClass("is-active");
      $('.menu__link').toggleClass("is-active animated faster fadeInUp");
      $('.header__logo').toggleClass("is-active");

      if ($(this).attr('aria-expanded') === 'true') {
        $(this).attr('aria-expanded', 'false');
      } else {
        $(this).attr('aria-expanded', 'true');
      }
    });

    //links
    $('a[href*="#"]').on('click', function (e) {
      e.preventDefault();
    
      $('.menu-mobile__btn, .menu__list, .header__logo').removeClass("is-active");
      $('.menu-mobile-box').removeClass("is-active d-flex animated faster fadeIn");
      $('.menu__link').removeClass("is-active animated faster fadeInUp");
      $('body').removeClass("lock-scroll");

      $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top,
      }, 500, 'linear');
    });

    //Load Structure Galleries items
    $('.structure__gallery__categories-item').click(function () {
      var category = $(this).data('category');
      
      // eslint-disable-next-line no-console
      //console.log(servico);

      //List
      $(".structure__gallery__categories-item").removeClass('current');
      $(this).addClass('current');
      
      //Images
      if (category == "0") {
        $(".structure__gallery__category").fadeIn(400);
      } else {
        $(".structure__gallery__category").fadeOut(400);
        $(".structure__gallery__category--"+category).fadeIn(400);
      }
    })

    //fancybox
    $('[data-fancybox="structure"]').fancybox({
      // Options will go here:
      protected: true,
      modal: true,
      animationEffect: "fade",
      hash: false,
      hideScrollbar: true,
      lang: "pt_br",
      i18n: {
        pt_br: {
          CLOSE: "Fechar",
          NEXT: "Próxima",
          PREV: "Anterior",
          ERROR: "O conteúdo solicitado não pode ser carregado. <br/> Por favor, tente novamente mais tarde.",
          PLAY_START: "Começar apresentação de slides",
          PLAY_STOP: "Pausar apresentação de slides",
          FULL_SCREEN: "Tela Cheia",
          THUMBS: "Miniaturas",
          DOWNLOAD: "Download",
          SHARE: "Compartilhar",
          ZOOM: "Zoom",
        },
      },
    });

    $('[data-fancybox="photos"]').fancybox({
      // Options will go here:
      protected: true,
      loop: true,
      animationEffect: "fade",
      hash: false,
      lang: "pt_br",
      i18n: {
        pt_br: {
            CLOSE: "Fechar",
            NEXT: "Próxima",
            PREV: "Anterior",
            ERROR: "O conteúdo solicitado não pode ser carregado. <br/> Por favor, tente novamente mais tarde.",
            PLAY_START: "Começar apresentação de slides",
            PLAY_STOP: "Pausar apresentação de slides",
            FULL_SCREEN: "Tela Cheia",
            THUMBS: "Miniaturas",
            DOWNLOAD: "Download",
            SHARE: "Compartilhar",
            ZOOM: "Zoom",
        },
      },
    });
    
    $('[data-fancybox="structure-gallery"]').fancybox({
      // Options will go here:
      protected: true,
      selector : '[data-fancybox="structure-gallery"]:visible',
      loop: true,
      idleTime: false,
      animationEffect: "fade",
      hash: false,
      lang: "pt_br",
      i18n: {
        pt_br: {
            CLOSE: "Fechar",
            NEXT: "Próxima",
            PREV: "Anterior",
            ERROR: "O conteúdo solicitado não pode ser carregado. <br/> Por favor, tente novamente mais tarde.",
            PLAY_START: "Começar apresentação de slides",
            PLAY_STOP: "Pausar apresentação de slides",
            FULL_SCREEN: "Tela Cheia",
            THUMBS: "Miniaturas",
            DOWNLOAD: "Download",
            SHARE: "Compartilhar",
            ZOOM: "Zoom",
        },
      },
    });
    
    $('.menu__link-tour').on('click', function () {
      var home_url = window.location.protocol + "//" + window.location.host; 
      var tour_url = home_url + "/app/tour/index.html";
      var blank_url = home_url + "/app/tour/blank.html";
      
      //hide menu-mobile
      $('.menu-mobile__btn, .menu__list, .header__logo').removeClass("is-active");
      $('.menu-mobile-box').removeClass("is-active d-flex animated faster fadeIn");
      $('.menu__link').removeClass("is-active animated faster fadeInUp");
      $('body').removeClass("lock-scroll");
      
      $.fancybox.open({
        src  : '#tour__modal',
        type : 'inline',
        opts : {
          protected: true,
          modal: true,
          animationEffect: "fade",
          hash: false,
          beforeShow : function( ) {
            $('.tour__iframe').attr("src", tour_url);
          },
          afterClose : function( ) {
            $('.tour__iframe').attr("src", blank_url);
          },
          lang: "pt_br",
          i18n: {
            pt_br: {
              CLOSE: "Fechar",
              NEXT: "Próxima",
              PREV: "Anterior",
              ERROR: "O conteúdo solicitado não pode ser carregado. <br/> Por favor, tente novamente mais tarde.",
              PLAY_START: "Começar apresentação de slides",
              PLAY_STOP: "Pausar apresentação de slides",
              FULL_SCREEN: "Tela Cheia",
              THUMBS: "Miniaturas",
              DOWNLOAD: "Download",
              SHARE: "Compartilhar",
              ZOOM: "Zoom",
            },
          },
        },
      });
    });

    //Google Tag
    $('.wpcf7').submit(function () {
      window.ga('send', 'event', 'Lead', 'Formulário', 'Contato');
    });

    // JQuery mask
    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
      onKeyPress: function (val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
    };

    $('.sp_celphones').mask(SPMaskBehavior, spOptions);
  },
};
