<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-disable-trackbacks');
    #add_theme_support('soil-google-analytics', 'UA-XXXXX-Y', 'wp_footer');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    add_image_size('lepartie-profile', 320, 320, true);
    add_image_size('lepartie-small-square', 640, 640, true);
    add_image_size('lepartie-medium-square', 960, 960, true);
    add_image_size('lepartie-medium', 1366);
    add_image_size('lepartie-large', 1920);
    add_image_size('lepartie-extra-large', 3840);

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Form Contato', 'dominiodobrasil'),
        'id'            => 'sidebar-contact-form'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Custom login page
 */
add_action('login_head', function () {
    echo '<link rel="stylesheet" type="text/css" href="' . asset_path('styles/wp-login.css') . '"/>';
    remove_action('login_head', 'wp_shake_js', 12);
});

add_filter('login_headerurl', function () {
    return get_bloginfo('url');
});

add_filter('login_headertitle', function () {
    return get_bloginfo('name');
});

/**
 * Hide content editor
 */
add_action('init', function () {
    // If not in the admin, return.
    if (!is_admin()) {
        return;
    }

    // Get the post ID on edit post with filter_input super global inspection.
    $current_post_id = filter_input(INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT);
    // Get the post ID on update post with filter_input super global inspection.
    $update_post_id = filter_input(INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT);

    // Check to see if the post ID is set, else return.
    if (isset($current_post_id)) {
        $post_id = absint($current_post_id);
    } else if (isset($update_post_id)) {
        $post_id = absint($update_post_id);
    } else {
        return;
    }

    // Don't do anything unless there is a post_id.
    if (isset($post_id)) {
        // Get the template of the current post.
        
        $frontpage = get_the_title($post_id);
        if ($frontpage == 'Home') {
            remove_post_type_support('page', 'editor');
        }

        $template_file = get_post_meta($post_id, '_wp_page_template', true);
        if ($template_file == 'views/front-page.blade.php') {
            remove_post_type_support('page', 'editor');
            // Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
        }
    }
});
